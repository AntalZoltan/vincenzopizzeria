package tablemodel;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.PizzaDough;

public class DoughTableModel extends AbstractTableModel{
    private static final String[] COLUMNS_NAME = new String[]{"Név","Ár"};
    private List<PizzaDough> doughs;

    public DoughTableModel(List<PizzaDough> dough) {
        this.doughs = dough;
        
    }
    
    public void addRow(PizzaDough dough){
        doughs.add(dough);
        int row = doughs.size()-1;
        fireTableRowsInserted(row,row);
    }
    public void deleteRow(PizzaDough dough){
        int row = doughs.indexOf(dough);
        doughs.remove(dough); 
        fireTableRowsDeleted(row, row);
    }
    public void updateRow(PizzaDough dough){
        int row = doughs.indexOf(dough);
        fireTableRowsUpdated(row, row);
    }
    public void updatePrice(PizzaDough dough, Double newPrice){
        int row = doughs.indexOf(dough);
        //doughs.get(row).setPrice(newPrice);
        fireTableRowsUpdated(row, row);
    }

    @Override
    public String getColumnName(int column){
            return COLUMNS_NAME[column];
    }   
    
    @Override
    public int getRowCount() {
        return doughs.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMNS_NAME.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case(0) : return doughs.get(rowIndex).getName();
            case(1) : return doughs.get(rowIndex).getPrice();
            default: return null;
        }
    }
    
}

