package tablemodel;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.BasketEntity;

public class BasketTableModel extends AbstractTableModel {

	private static final String[] COLUMNS_NAME = new String[]{"Darabszám","Méret","Pizza tészta","Pizza alap","Pizza feltétek","Extrák","Ár"};
	private List<BasketEntity> basket;
	
	public BasketTableModel(List<BasketEntity> basket){
		this.basket = basket;
        }
        
	@Override
	public int getRowCount() {
            return basket.size();
	}

	@Override
	public int getColumnCount() {
            return COLUMNS_NAME.length;
	}

	@Override
	public Object getValueAt(int row, int column) {
            BasketEntity basketEntity = basket.get(row);
            switch(column){
                case(0) : return basketEntity.getNumberOfPizza();
                case(1) : return basketEntity.getSize();
                case(2) : return basketEntity.getDough();
                case(3) : return basketEntity.getBase();
                case(4) : return basketEntity.getToppingsFormatted();
                case(5) : return basketEntity.getExtrasFormatted();
                case(6) : return basketEntity.getPriceOfPizza();
                default : return null;
            }
	}
	
	@Override
	public String getColumnName(int column){
		return COLUMNS_NAME[column];
	}
        
         public void addRow(BasketEntity basketEntity){
            basket.add(basketEntity);
            int row = basket.size()-1;
            fireTableRowsInserted(row,row);
        }
        public void deleteLastRow(){
            int lastRow = basket.size()-1;
            basket.remove(basket.get(lastRow)); 
            fireTableRowsDeleted(lastRow, lastRow);
        }
        
        public void clearBasket(){
            basket.clear();
            fireTableDataChanged();
        }
        
        public boolean isEmpty(){
            if(basket.size()==0 || basket==null){
                return true;
            }
            return false;
        }
}
