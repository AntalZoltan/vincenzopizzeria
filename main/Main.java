package main;

import model.PizzaDough;
import service.PizzaDataBaseService;
import service.PizzaService;

public class Main {
    public static void main(String[] args) {
        PizzaDataBaseService pdbs = new PizzaDataBaseService();
        PizzaService ps = new PizzaService();
        pdbs.initPizzaDataBase();
        /*
        A program leges legelső futása után a pdbs.savaUsers() és a pdbs.saveMaterials() metódusokat akár ki is lehet kommentelni.
        */
        pdbs.saveUsers();
        pdbs.savePizzaMaterials();
        ps.makeGUI();
    }
}
