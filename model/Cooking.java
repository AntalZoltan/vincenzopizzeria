package model;

public interface Cooking {
    
    public double getCookingTime(PizzaDough dough);
    
}
