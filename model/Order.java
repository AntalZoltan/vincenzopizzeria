package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Order implements Serializable, Comparable<Order>{
    
    private static int initOrderId = 1000;
    private final int orderId;
    private User user;
    private LocalDate dateOfOrder;
    private String note;
    private double priceOfOrder;
    private int pizzaQuantity;
    private int size;
    private PizzaDough dough;
    private PizzaBase base;
    private ArrayList<PizzaTopping> toppings;
    private String[] extras = new String[2];
    
    public static Map<Integer,Order> orders = new HashMap<>();

    public Order(int orderId, User user, LocalDate dateOfOrder, String note, double priceOfOrder, int pizzaQuantity, int size, PizzaDough dough, PizzaBase base, ArrayList<PizzaTopping> toppings, String[] extras) {
        this.orderId = orderId;
        this.user = user;
        this.dateOfOrder = dateOfOrder;
        this.note = note;
        this.priceOfOrder = priceOfOrder;
        this.pizzaQuantity = pizzaQuantity;
        this.size = size;
        this.dough = dough;
        this.base = base;
        this.toppings = toppings;
        this.extras = extras;
    }
    
    
    //Order hozzáadása a MAP-hez példány változók felhasználásával
    public static void addOrderToMap(int orderId, User user, LocalDate dateOfOrder, String note, double priceOfOrder, int pizzaQuantity, int size, PizzaDough dough, PizzaBase base, ArrayList<PizzaTopping> toppings, String[] extras){
        Order o = Order.orders.get(orderId);
        //Ha nincs akkor létrehozunk egy újat és beletesszük a Map-be.
        if(o==null){
            o=new Order(orderId, user, dateOfOrder, note, priceOfOrder, pizzaQuantity, size, dough, base, toppings, extras);
            Order.orders.put(orderId, o);
        }
    }
    
    //Order hozzáadása a MAP-hez objektum felhasználásával
    public static void addOrderToMap(int orderId, Order order){
        Order o = Order.orders.get(orderId);
        //Ha nincs akkor létrehozunk egy újat és beletesszük a Map-be.
        if(o==null){
            o=new Order(orderId, order.getUser(), order.getDateOfOrder(), order.getNote(), order.getPriceOfOrder(), order.getPizzaQuantity(), order.getSize(), order.getDough(), order.getBase(), order.getToppings(), order.getExtras());
            Order.orders.put(orderId, o);
        }
    }

    //Order törlése a MAP-ből
    public static void removeOrder(Order order){
        orders.remove(order.getOrderId(), order);
    }
    //Egy Order megszerzése
    public static Order getOrder(int orderId){
        Order o = Order.orders.get(orderId);
        return o;
    }
    //Order tipusú lista visszaadása
    public static Collection<Order> getOrders(){
	return orders.values();
    }
    //Az adott Order toppings listájához hozzáadunk egy topping-ot.
    public void addTopping(String name, double price){
        toppings.add(new PizzaTopping(name, price));
    }
    
    //Orderek összehasonlitása
    @Override
    public int compareTo(Order o) {
        int result;
        if(getOrderId() > o.getOrderId()){
            result = 1;
        }else if(getOrderId() < o.getOrderId()){
            result = -1;
        }else{
            result = 0;
        }
        return result;
    }

    //Szerializálás, objektumok kiirása
    //A Map-et irjuk ezzel ki.
    public static void saveOrders(Map<Integer, Order> map){
        File f = new File("src/db/orders.dat");//Lérehozunk egy fájl objektumot, ami vár egy Stringet, hogy a fájl hol helyezkedik el.
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);//Létrehozunk egy kiiró Stream-et.
            ObjectOutputStream oos = new ObjectOutputStream(fos); //Létrehozunk egy objektum kiirót,
            oos.writeObject(map); //és átadjuk neki a kiirandó Map-et.
        } catch (IOException e) {
            e.printStackTrace();
        } finally{
            if(fos!=null){
                try {
                        fos.close(); //A fájlt mindenképp le kell zárni.
                } catch (IOException e) {
                        e.printStackTrace();
                }
            }
        }
    }
	
    //Deszerializálás, objektumok visszaolvasása
    //Map-et olvasunk be.
    public static void loadOrders(Map<Integer, Order> map){
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("src/db/orders.dat");//Látrehozunk egy olyan Stream-et, amiből lehet olvasni.
            ObjectInputStream ois = new ObjectInputStream(fis);//Létrehozunk egy objektum olvasót,
            map = (Map<Integer, Order>)ois.readObject();//és a Map-be tesszük a kiolvasott objektumot, Map-re kasztolva.
            Order.orders = (Map<Integer, Order>) map;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally{
            if(fis!=null){
                try {
                        fis.close();//A fájlt mindenképp le kell zárni.
                } catch (IOException e) {
                        e.printStackTrace();
                }
            }
        }
    }
    
    public StringBuilder getToppingsFormatted() {
        if(toppings.size()!=0){
            StringBuilder sbToppings = new StringBuilder();
            for(int i = 0; i < toppings.size(); i++) {
                if(toppings.get(i)!=null){
                    sbToppings.append(toppings.get(i));
                    if(i!=toppings.size()-1) sbToppings.append(" ");
                }
            }
            return sbToppings;
        }
        return null;
    }
    
    public StringBuilder getExtrasFormatted() {
        if(extras.length!=0){
            StringBuilder sbExtras = new StringBuilder();
            for(int i = 0; i < extras.length; i++) {
                if(extras[i]!=null){
                    sbExtras.append(extras[i]);
                    if(i!=extras.length-1) sbExtras.append(" ");
                }
            }
            return sbExtras;
        }
        return null;
    }
    
    public static int getInitOrderId() {
        return initOrderId;
    }
    
    public int getOrderId() {
        return orderId;
    }
    
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
    public LocalDate getDateOfOrder() {
        return dateOfOrder;
    }

    public void setDateOfOrder(LocalDate dateOfOrder) {
        this.dateOfOrder = dateOfOrder;
    }

    public double getPriceOfOrder() {
        return priceOfOrder;
    }

    public void setPriceOfOrder(double priceOfOrder) {
        this.priceOfOrder = priceOfOrder;
    }
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getPizzaQuantity() {
        return pizzaQuantity;
    }

    public void setPizzaQuantity(int pizzaQuantity) {
        this.pizzaQuantity = pizzaQuantity;
    }

    public PizzaDough getDough() {
        return dough;
    }

    public void setDough(PizzaDough dough) {
        this.dough = dough;
    }

    public PizzaBase getBase() {
        return base;
    }

    public void setBase(PizzaBase base) {
        this.base = base;
    }
    
    public ArrayList<PizzaTopping> getToppings() {
        return (ArrayList<PizzaTopping>) toppings;
    }

    public void setToppings(ArrayList<PizzaTopping> toppings) {
        this.toppings = toppings;
    }
    
    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String[] getExtras() {
        return extras;
    }
    
    public void setExtras(String[] extras) {
        this.extras = extras;
    }

    @Override
    public String toString() {
        return "\n\nRendelés szám: "+orderId+"\n"+ pizzaQuantity + " db pizza (egységár: " + (priceOfOrder / pizzaQuantity) + " Ft)\nPizza tészta: " + dough + "\nPizza alap: " + base + "\nFeltétek:" + toppings + "\nÖsszesen: " + priceOfOrder + " Ft";
    }
    
    
    
}
