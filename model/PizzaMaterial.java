package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Map;

public abstract class PizzaMaterial implements Serializable, Comparable<PizzaMaterial>{

    private String name;
    private double price;

    public PizzaMaterial(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return getName();
    }
    
    
    //Szerializálás, objektumok kiirása
    //A Map-et irjuk ezzel ki.
    public static void savePizzaMaterials(Map<String, ?> map, String savedName){
        File f = new File("src/db/"+savedName+".dat");//Lérehozunk egy fájl objektumot, ami vár egy Stringet, hogy a fájl hol helyezkedik el.
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);//Létrehozunk egy kiiró Stream-et.
            ObjectOutputStream oos = new ObjectOutputStream(fos); //Létrehozunk egy objektum kiirót,
            oos.writeObject(map); //és átadjuk neki a kiirandó Map-et.
        } catch (IOException e) {
            e.printStackTrace();
        } finally{
            if(fos!=null){
                try {
                        fos.close(); //A fájlt mindenképp le kell zárni.
                } catch (IOException e) {
                        e.printStackTrace();
                }
            }
        }
    }
	
    //Deszerializálás, objektumok visszaolvasása
    //Map-et olvasunk be.
    public static void loadPizzaMaterials(Map<String, ?> map, String savedName) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("src/db/"+savedName+".dat");//Látrehozunk egy olyan Stream-et, amiből lehet olvasni.
            ObjectInputStream ois = new ObjectInputStream(fis);//Létrehozunk egy objektum olvasót,
            map = (Map<String, ?>)ois.readObject();//és a Map-be tesszük a kiolvasott objektumot, Map-re kasztolva.
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally{
            if(fis!=null){
                try {
                        fis.close();//A fájlt mindenképp le kell zárni.
                } catch (IOException e) {
                        e.printStackTrace();
                }
            }
        }
        switch (savedName) {
            case "doughs" : PizzaDough.doughs = (Map<String, PizzaDough>) map; break;
            case "bases" : PizzaBase.bases = (Map<String, PizzaBase>) map; break;
            case "toppings" : PizzaTopping.toppings = (Map<String, PizzaTopping>) map; break;
        }
    }

}
