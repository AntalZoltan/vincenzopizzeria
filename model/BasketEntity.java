package model;

import java.util.ArrayList;

public class BasketEntity {
    
    private int numberOfPizza;
    private int size;
    private PizzaDough dough;
    private PizzaBase base;
    private ArrayList<PizzaTopping> toppings;
    private String[] extras;
    private double priceOfPizza;

    public BasketEntity(int numberOfPizza, int size, PizzaDough dough, PizzaBase base, ArrayList<PizzaTopping> topping, String[] extras, double priceOfPizza) {
        this.numberOfPizza = numberOfPizza;
        this.size = size;
        this.dough = dough;
        this.base = base;
        this.toppings = topping;
        this.extras = extras;
        this.priceOfPizza = priceOfPizza;
    }
    
    public StringBuilder getToppingsFormatted() {
        if(toppings.size()!=0){
            StringBuilder sbToppings = new StringBuilder();
            for(int i = 0; i < toppings.size(); i++) {
                if(toppings.get(i)!=null){
                    sbToppings.append(toppings.get(i));
                    if(i!=toppings.size()-1) sbToppings.append(" ");
                }
            }
            return sbToppings;
        }
        return null;
    }
    public StringBuilder getExtrasFormatted() {
        if(extras.length!=0){
            StringBuilder sbExtras = new StringBuilder();
            for(int i = 0; i < extras.length; i++) {
                if(extras[i]!=null){
                    sbExtras.append(extras[i]);
                    if(i!=extras.length-1) sbExtras.append(" ");
                }
            }
            return sbExtras;
        }
        return null;
    }

    public int getNumberOfPizza() {
        return numberOfPizza;
    }

    public void setNumberOfPizza(int numberOfPizza) {
        this.numberOfPizza = numberOfPizza;
    }

    public PizzaDough getDough() {
        return dough;
    }

    public void setDough(PizzaDough dough) {
        this.dough = dough;
    }

    public PizzaBase getBase() {
        return base;
    }

    public void setBase(PizzaBase base) {
        this.base = base;
    }

    public ArrayList<PizzaTopping> getTopping() {
        return toppings;
    }

    public void setTopping(ArrayList<PizzaTopping> topping) {
        this.toppings = topping;
    }

    public double getPriceOfPizza() {
        return priceOfPizza;
    }

    public void setPriceOfPizza(double priceOfPizza) {
        this.priceOfPizza = priceOfPizza;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String[] getExtras() {
        return extras;
    }

    public void setExtras(String[] extras) {
        this.extras = extras;
    }
}
