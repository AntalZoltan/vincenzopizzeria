Online pizzéria rendelési applikáció (Vincenzo’s Pizza Ltd.) feladat specifikáció.

Milánó külvárosában nem rég nyitott pizzéria vezetője arra kért fel minket, hogy egy on-line pizza rendelési programot fejlesszünk ki számukra. 
Vincenzonak hívják a tulajdonost, 3 gyermeke van, imádják a pizzát, de nagyon válogatósak. 
Az egyik nem szereti a paradicsomos alapot, a másik a gombát, a harmadik pedig minden pizzának csak a szélét eszi meg. 

Eszébe jutott, hogy mi lenne, ha azzal emelkedne ki a versenytársak közül, hogy a pizzákat tetszés szerint lehessen összeállítani a tésztáktól az alapokon át a feltétekig, ahogy a felhasználó kéri.

A pizzákat lehessen rendelni a legújabb trendek szerint vékony tésztával, de természetesen normál és vastag tésztával is. 
Vincenzo már régi motoros a szakmában. Tudja, hogy a tészták vastagságától függően a pizzák elkészítéséhez más és más sütési idő szükséges, amit fontos jelezni a felhasználó felé, hogy az tudja, hogy mennyi idő múlva kapja meg a rendelését.

További újjutásokat is tervez Vincenzo. 
A pizzákat lehessen dupla sajttal kérni, és gondolkodik a csökkentett szénhidrát tartalmú tészták felhasználásán is. 

Jelenleg 5-6 pizza alapot (paradicsomos, fokhagymás, stb.) és 7-8 pizza feltétet (hagyma, sonka, gomba, kukorica, ananász, bacon, stb.) használ. 
Szeretné később bővíteni a kínálatát, akár több alappal, akár több feltéttel. 

Ehhez szükséges egy adminisztrációs munkatárs, aki majd be tud jelentkezni a programba felhasználónév-jelszóval, és változtatni tudja a rendelhető pizza alapanyagokat.

További kívánsága a pizzéria vezetőjének, hogy az alkalmazásba a felhasználók is be tudjanak jelentkezni felhasználónév-jelszóval. 
A bejelentkezett felhasználók rendeléseit a program tárolja el, hogy belépéskor lássák mit rendeltek régebben. 

Továbbá arra számit, hogy visszajelzést adnak a rendelésükről. 
Ebből tudja majd, hogy melyik pizza alapanyagot érdemes a rendelési programban megtartani és melyiket kivenni. Illetve mely új és trendi pizza alapanyagokat kell felvenni. 

Vincenzo tudja, hogy sokan csak látogatóként rendelnének pizzát, személyes adataik eltárolása nélkül, ezért szeretné, ha bejelentkezés nélkül is lehetne rendelést leadni. 
Ebben az esetben sem adatokat, sem rendeléseket nem szükséges eltárolni.

Természetesen a pizzák ára is fontos. 
Vincenzo arra készül, hogy egységárban adja a pizzákat maximum három feltét kiválasztása esetén. 
Az egységárat 1.400 Ft-ban határozná meg. 
Minden további feltét kiválasztása esetén feltétenként plusz díjat számolna fel. 2.000 Ft-os rendelés felett pedig ajándékkal kedveskedve a felhasználóknak.

Egy rendelés során több darab pizzát is lehessen összeállítani, akár egymástól különbözőeket is. 
Az összeállítás közben a program tájékoztassa a felhasználót a rendelés addigi összegéről. 
Amikor a felhasználó már minden rendelését összeállította, akkor a program jelezze ki a végösszeget, mielőtt a felhasználó véglegesítené és rányomna a rendelés gombra. 
Előfordulhat olyan eset is, amikor a felhasználó hibás rendelést állított össze, de még nem véglegesítette, ebben az esetben lehessen törölni a rendelést. 

Vincenzo az online pizza rendelési program felületét szeretné könnyen kezelhetőnek, felhasználó barátnak látni. 
Szeretné, ha a pizza alapanyagok kiválasztható listákba szerveződnének, hogy a felhasználóknak szinte csak kattintani kelljen, és minél kevesebbet gépelni. 

Megfordult a fejében, hogy a pizza alapanyagok kiválasztása közben grafikusan is megjeleníti a pizzát, de még nem döntötte el, hogy valóban akarja-e.

Vincenzo reméli, hogy az új pizza rendelési applikáció segítségével, kiemelkedhet a többi pizzéria közül, új felhasználókat csábíthat a pizzériához és magas hozamot érhet el. 
Az applikáció elkészítéséért a fejlesztőknek magas jutalmazást ígér.
